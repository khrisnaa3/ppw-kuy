from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from lab10 import views
from lab10.models import user_list


class testing_response_page(TestCase):

    def test_page_landing(self):
        response = Client().get('/user')
        self.assertEqual(response.status_code, 301)

    def test_page_login(self):
        response = Client().get('/user/login')
        self.assertEqual(response.status_code, 200)

    def test_page_logout(self):
        response = Client().get('/user/logout')
        self.assertEqual(response.status_code, 302)

    def test_rest_api_framework(self):
        response = Client().get('/user/api/users')
        self.assertEqual(response.status_code, 200)

class testing_function_in_views(TestCase):

    def test_login(self):
        found = resolve('/user/login')
        self.assertEqual(found.func, views.LoginRequest)

    def test_logout(self):
        found = resolve('/user/logout')
        self.assertEqual(found.func, views.logout_view)

    def test_email_validation(self):
        found = resolve('/user/validation')
        self.assertEqual(found.func, views.email_validation)

    def test_subscriber(self):
        found = resolve('/user/subscriber')
        self.assertEqual(found.func, views.UserDetail)

    def test_registration(self):
        found = resolve('/user/registered')
        self.assertEqual(found.func, views.add_registration)

    def test_home(self):
        response = Client().get('/user/')
        found = resolve('/user/')
        self.assertEqual(found.func, views.register_page)
        self.assertTemplateUsed(response, 'landing.html')

    def test_register(self):
        request = HttpRequest()
        response = views.add_registration(request)
        self.assertEqual(response.status_code, 200)

    def test_model(self):
        new = user_list.objects.create(username='bejo', email='bejo@mail.com', password='bejo123')
        modelCount = user_list.objects.all().count()
        self.assertEqual(modelCount, 1)

