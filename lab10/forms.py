from django import forms

class register_form(forms.Form):
    username = forms.CharField(widget=forms.TextInput({'class': 'form-control'}), required=True)
    email = forms.EmailField(widget=forms.EmailInput({'class': 'form-control'}), required=True)
    password = forms.CharField(widget=forms.PasswordInput({'class': 'form-control'}), required=True)

class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput({'class': 'form-control'}), required=True)
    password = forms.CharField(widget=forms.PasswordInput({'class': 'form-control'}), required=True)