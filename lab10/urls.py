from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    path('', views.register_page),
    path('registered', views.add_registration),
    path('api/users', views.user_api.as_view()),
    path('validation', views.email_validation),
    path('subscriber', views.UserDetail),
    path('login', views.LoginRequest),
    path('logout', views.logout_view),
]

urlpatterns = format_suffix_patterns(urlpatterns)
