from django.contrib.auth import authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from rest_framework import generics
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from lab10.forms import register_form, LoginForm
from lab10.models import user_list
from lab10.serializer import user_serializer
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login


def register_page(request):
    form = register_form();
    response = {'form': form}
    return render(request, 'landing.html', response)


response = {}


def add_registration(request):
    if request.method == "POST":
        form = register_form(request.POST or None)
        if form.is_valid():
            response['username'] = request.POST['username']
            response['email'] = request.POST['email']
            response['password'] = request.POST['password']

            user = User.objects.create_user(username=response['username'], email=response['email'],
                                            password=response['password'])
            user.save()
            return render(request, 'landing.html', response)
        else:
            return HttpResponseRedirect('/user/login')
    else:
        return render(request, 'landing.html', response)


@csrf_exempt
def email_validation(request):
    email = request.POST.get('email', None)
    data = {
        'is_taken': user_list.objects.filter(email=email).exists()
    }
    return JsonResponse(data)


class user_api(APIView):

    def get(self, request):
        user_lists = user_list.objects.all()
        serializer = user_serializer(user_lists, many=True)
        return Response(serializer.data)


class UserDetail(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a given user.
    """
    queryset = user_list.objects.all()
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return Response({'user': self.object}, template_name='user_detail.html')


def LoginRequest(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/ppwinaja/books')
    if request.method == 'POST':
        form2 = LoginForm(request.POST or None)
        if form2.is_valid():
            username = form2.cleaned_data['username']
            password = form2.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/ppwinaja/books')
            else:
                return HttpResponseRedirect('/user/login')
        else:
            context = {'form_login': form2}
            return render(request, 'login.html', context)
    else:
        form2 = LoginForm(request.POST or None)
        context = {'form_login': form2}
        return render(request, 'login.html', context)


def logout_view(request):
    logout(request)
    request.session.flush()
    return HttpResponseRedirect('/user/login')
