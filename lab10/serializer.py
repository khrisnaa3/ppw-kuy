from rest_framework import serializers
from . models import user_list

class user_serializer(serializers.ModelSerializer):

    class Meta:
        model = user_list
        fields = ('username', 'email')