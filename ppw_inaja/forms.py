from django import forms


class homeForm(forms.Form):
    status = forms.CharField(widget=forms.TextInput({'class': 'form-control'}), max_length=300)


class searching(forms.Form):
    search = forms.CharField(label='Search Other Books', required=True, max_length=100,
                             widget=forms.TextInput(attrs={'class': 'form-control'}))
