from django.urls import path
from ppw_inaja import views

urlpatterns = [
    path('', views.utama),
    path('status', views.homepage),
    path('addform', views.addform),
    path('profile', views.profile),
    path('jsonbooks', views.dataJson),
    path('books', views.ajaxBooks),
    path('caribuku', views.cariBuku),
]
