import time
import unittest

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Status
from . import views


# Create your tests here.

class testing_homepage(TestCase):

    def test_homepage(self):
        response = Client().get('/ppwinaja/status')
        self.assertEqual(response.status_code, 200)


    def test_addform(self):
        response = Client().get('/ppwinaja/addform')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addform.html')
        html_response = response.content.decode('utf8')
        self.assertIn('<title>Add Form</title>', html_response)
        self.assertIn('<h2>Status berhasil diunggah</h2>', html_response)

    def test_profile(self):
        response = Client().get('/ppwinaja/profile')
        self.assertEqual(response.status_code, 200)

    def test_homepage_func(self):
        found = resolve('/ppwinaja/status')
        self.assertEqual(found.func, views.homepage)

    def test_addform_func(self):
        found = resolve('/ppwinaja/addform')
        self.assertEqual(found.func, views.addform)

    def test_form(self):
        request = HttpRequest()
        response = views.homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form id="form" method="POST" action="/ppwinaja/addform">', html_response)

    def test_submit_button(self):
        request = HttpRequest()
        response = views.homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<button type="submit">', html_response)

    def test_auto_redirect(self):
        request = HttpRequest()
        response = views.addform(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<meta http-equiv="refresh"', html_response)

    def test_model_new_activity(self):
        Status.objects.create(status="Ini Status Test")
        all_activities = Status.objects.all().count()
        self.assertEqual(all_activities, 1)

    def test_profile_template(self):
        response = Client().get('/ppwinaja/profile')
        self.assertTemplateUsed(response, 'profile.html')
        html_response = response.content.decode('utf8')
        self.assertIn('<title>Stefanus Khrisna A.H</title>', html_response)

    def test_profile_func(self):
        found = resolve('/ppwinaja/profile')
        self.assertEqual(found.func, views.profile)


# Jalankan localhost terlebih dahulu untuk melakukan manage.py test
class SubmitStatusTest(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # pakai ('./blabla') karena pada saat run manage.py , manage.py berada di satu atas current folder ini
        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(SubmitStatusTest, self).setUp()

    def tearDown(self):
        time.sleep(2)
        self.driver.quit()
        super(SubmitStatusTest, self).tearDown()

    def test_can_submit_status(self):
        self.driver.get('http://localhost:8000/ppwinaja/status')
        self.assertIn('Homepage', self.driver.title)
        header_text = self.driver.find_element_by_tag_name('h1').text
        self.assertIn('Halo, apa kabar ?', header_text)
        time.sleep(2)
        search_box = self.driver.find_element_by_id('id_status')
        search_box.send_keys('coba coba')
        search_box.submit()
        time.sleep(3)


    def test_check_css(self):
        self.driver.get('http://localhost:8000/ppwinaja/status')
        back_color = self.driver.find_element_by_tag_name('body').value_of_css_property('background-color')
        font_family = self.driver.find_element_by_tag_name('h4').value_of_css_property('font-family')
        self.assertEqual('rgba(240, 255, 255, 1)', back_color)
        self.assertEqual('"Comic Sans MS"', font_family)

    def test_positioning(self):
        self.driver.get('http://localhost:8000/ppwinaja/status')
        form_size = self.driver.find_element_by_tag_name('form').size
        margin_body = self.driver.find_element_by_tag_name('body').value_of_css_property("margin-left")
        self.assertEqual(500, form_size['width'])
        self.assertEqual('550px', margin_body)


class jsonBooks_test(TestCase):

    def test_url_books_exist(self):
        response = Client().get('/ppwinaja/jsonbooks')
        self.assertEqual(response.status_code, 200)

    def test_url_jsonData_exist(self):
        response = Client().get('/ppwinaja/jsonbooks')
        self.assertEqual(response.status_code, 200)

    def test_renderJson_func(self):
        found = resolve('/ppwinaja/jsonbooks')
        self.assertEqual(found.func, views.dataJson)

    def test_dataJson_func(self):
        found = resolve('/ppwinaja/books')
        self.assertEqual(found.func, views.ajaxBooks)
