import datetime
import json
import requests
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .forms import homeForm, searching
from .models import Status


def index(request):
    return render(request, 'hello.html')


# Create your views here.
def homepage(request):
    form = homeForm()
    date = datetime.datetime.now()
    response = {'form': form, 'get_all_status': Status.objects.all().order_by('-time'), 'datetime': date}
    return render(request, 'homepage.html', response)


def utama(request):
    return HttpResponseRedirect('/ppwinaja/books')


response = {}


def addform(request):
    if request.method == 'POST':
        form = homeForm(request.POST or None)
        if form.is_valid():
            response['status'] = request.POST['status']
            status = Status(status=response['status'])
            status.save()
            return render(request, 'addform.html', response)

    else:
        return render(request, 'addform.html')


def profile(request):
    return render(request, 'profile.html')


url_json = "https://www.googleapis.com/books/v1/volumes?q=quilting"


@login_required
@csrf_exempt
def ajaxBooks(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/user/login')

    user = request.user
    print(request.session.session_key)
    # print(request.session['jml'])

    if request.method == 'POST':
        print("posted")
        request.session['jml'] = request.POST['jml']

    if user is not None:
        jml = request.session.get('jml')
        if jml is None:
            jml = 0

        return render(request, 'ajax.html', {'user': user, 'searching': searching, 'jumlah_fav': jml})

    jml = request.session.get('jml')
    if jml is None:
        jml = 0
    return render(request, 'ajax.html', {'user': user, 'searching': searching, 'jml_fav': jml})


def dataJson(request):
    jsonData = requests.get(url_json)
    data = {"data": (jsonData.json())["items"]}
    return HttpResponse(json.dumps(data), content_type="application/json")


def cariBuku(request):
    if request.method == 'POST':
        form = searching(request.POST or None)
        if form.is_valid():
            global url_json
            url_json = 'https://www.googleapis.com/books/v1/volumes?q=' + request.POST['search']
            return ajaxBooks(request)
