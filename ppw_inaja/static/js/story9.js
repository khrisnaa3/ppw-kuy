$(document).ready(function (e) {
    $('#books_table').DataTable({
        "ajax": {
            "dataType": 'json',
            "contentType": "application/json; charset=utf-8",
            "url": "/ppwinaja/jsonbooks",
        },
        "columns": [
            {"data": "volumeInfo.title"},
            {"data": "volumeInfo.authors[,]"},
            {
                orderable: false,
                data: "accessInfo.webReaderLink",
                render: function (data, type, row, meta) {
                    return "<a style='color: cornflowerblue' href='" + data + "'>Klik Disini</a>"
                }
            },
            {
                orderable: false,
                data: null,
                render: function (data, type, row, meta) {
                    return "<span class='fa fa-star'></span>"
                }
            }
        ],
    });
});

var counter = 0;

$(document).on('click', '.fa', function () {
    
    $(".favCounter").html(counter);

    if (this.className === "fa fa-star") {
        counter++;
        $(".favCounter").html(counter);
        $.post('/ppwinaja/books', {'jml' : counter});
        $(this).addClass("checked");
    }
    else {
        counter--;
        $(".favCounter").html(counter);
        $.post('/ppwinaja/books', {'jml' : counter});
        $(this).removeClass("checked");
    }
});
