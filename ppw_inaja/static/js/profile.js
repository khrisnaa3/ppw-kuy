$(document).ready(function () {
    $("#mengubah").click(function () {
        if ($(this).is(":checked")) {
            $("body").css("background-color", "#79aec8");
            $("h1").css("color", "red");
            $("label").css("color", "white");
        } else {
            $("body").css("background-color", "#f5dd5d");
            $("h1").css("color", "black");
            $("label").css("color", "black");
        }
    })
});

$(function () {
    $("#accordion").accordion({
        collapsible: true
    });
});

var waktuVar;

function loadFunction() {
    waktuVar = setTimeout(showPage, 3000);
}

function showPage() {
    document.getElementById("holder").style.display = "none";
    document.getElementById("konten").style.display = "block";
}
